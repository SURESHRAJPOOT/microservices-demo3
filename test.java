package com.example.tests;

import org.junit.Test;
import static org.junit.Assert.*;

public class EcommerceAppTest {

    @Test
    public void testUserRegistration_Red() {
        EcommerceApp app = new EcommerceApp();
        boolean registrationSuccess = app.registerUser("john@example.com", "password");
        assertFalse(registrationSuccess); // Expected failure, registration should not succeed
    }

    @Test
    public void testUserRegistration_Green() {
        EcommerceApp app = new EcommerceApp();
        boolean registrationSuccess = app.registerUser("john@example.com", "password");
        assertTrue(registrationSuccess); // Expected successful registration
    }

    @Test
    public void testPlaceOrder_Refactor() {
        EcommerceApp app = new EcommerceApp();
        app.login("john@example.com", "password");
        app.addItemToCart("12345", 2);
        boolean orderSuccess = app.placeOrder();
        assertTrue(orderSuccess); // Expected successful order placement after refactoring the code
    }

}
